import auth from './auth';
import expenses from './expenses';
import dogs from './dogs';
import common from './common';
import persist from './persist';

export default {
  ...auth,
  ...expenses,
  ...dogs,
  ...common,
  ...persist,
};
