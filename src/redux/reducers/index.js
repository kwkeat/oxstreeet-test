import auth from './auth';
import expenses from './expenses';
import dogs from './dogs';
import persist from './persist';

export default {
  AUTH: auth,
  EXPENSES: expenses,
  DOGS: dogs,
  PERSIST: persist,
};
