export const NAME = 'DOGS';

export const getDogsList = store => store[NAME].fetch.data;
