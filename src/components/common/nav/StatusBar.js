import React from 'react';
import {StyleSheet, View, Platform, Dimensions} from 'react-native';
import PropTypes from 'prop-types';

const {height: SCREEN_HEIGHT} = Dimensions.get('window');
const IS_IPHONE_X = SCREEN_HEIGHT >= 812;
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;

const StatusBar = ({statusBarStyle}) => (
  <View style={[styles.statusBar, statusBarStyle]} />
);

const styles = StyleSheet.create({
  statusBar: {
    height: STATUS_BAR_HEIGHT,
    backgroundColor: 'transparent',
  },
});

StatusBar.propTypes = {
  statusBarStyle: PropTypes.any,
};

StatusBar.defaultProps = {
  statusBarStyle: {},
};

export default StatusBar;
