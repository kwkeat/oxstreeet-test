import {takeLatest, all, fork, put, call} from 'redux-saga/effects';
import Actions from 'actions';
import firestore from '@react-native-firebase/firestore';
import AppNavigationService from 'navigator/app/AppNavigationService';

function* createExpenses({data}) {
  try {
    const ref = firestore().collection('expenses');
    const response = yield ref.add(data);
    if (response) {
      yield put(Actions.createExpensesSuccess());
      yield put(Actions.fetchExpenses());
      yield call(AppNavigationService.navigate, 'ExpenseList');
    }
  } catch (error) {
    yield put(Actions.createExpensesFail(error));
  }
}

function* watchCreateExpenses() {
  yield takeLatest(Actions.CREATE_EXPENSES, createExpenses);
}

export default function* create() {
  yield all([fork(watchCreateExpenses)]);
}
