export const NAME = 'EXPENSES';

export const CREATE_EXPENSES = `${NAME}/CREATE_EXPENSES`;
export const CREATE_EXPENSES_SUCCESS = `${NAME}/CREATE_EXPENSES_SUCCESS`;
export const CREATE_EXPENSES_FAIL = `${NAME}/CREATE_EXPENSES_FAIL`;

export const createExpenses = data => ({
  type: CREATE_EXPENSES,
  data,
});

export const createExpensesSuccess = () => ({
  type: CREATE_EXPENSES_SUCCESS,
});

export const createExpensesFail = error => ({
  type: CREATE_EXPENSES_FAIL,
  error,
});
