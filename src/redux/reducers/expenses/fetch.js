import Actions from 'actions';

const getDefaultState = () => ({isLoading: false, error: null, data: []});

function fetch(state, action) {
  if (typeof state === 'undefined') {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.FETCH_EXPENSES:
      return {
        isLoading: true,
        error: null,
        data: [],
      };
    case Actions.FETCH_EXPENSES_SUCCESS:
      return {
        isLoading: false,
        error: null,
        data: action.data,
      };
    case Actions.FETCH_EXPENSES_FAIL:
      return {
        isLoading: false,
        error: action.error,
        data: [],
      };
    default:
      return state;
  }
}

export default fetch;
