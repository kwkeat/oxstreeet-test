import {takeLatest, all, fork, put, call} from 'redux-saga/effects';
import Actions from 'actions';
import AppNavigationService from 'navigator/app/AppNavigationService';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

function* signUp({credentials}) {
  try {
    const token = yield auth().createUserWithEmailAndPassword(
      credentials.email,
      credentials.password,
    );
    if (token) {
      const ref = firestore()
        .collection('users')
        .doc(token.user._user.uid)
        .set(credentials);
      yield put(Actions.signUpSuccess(token));
      yield call(AppNavigationService.navigate, 'TabBar');
    }
  } catch (error) {
    yield put(Actions.signUpFail(error));
  }
}

function* watchSignUp() {
  yield takeLatest(Actions.SIGN_UP, signUp);
}

export default function* authentication() {
  yield all([fork(watchSignUp)]);
}
