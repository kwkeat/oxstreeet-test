import Actions from 'actions';

const getDefaultState = () => ({isLoading: false, error: null});

function fetch(state, action) {
  if (typeof state === 'undefined') {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.CREATE_EXPENSES:
      return {
        isLoading: true,
        error: null,
      };
    case Actions.CREATE_EXPENSES_SUCCESS:
      return {
        isLoading: false,
        error: null,
      };
    case Actions.CREATE_EXPENSES_FAIL:
      return {
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export default fetch;
