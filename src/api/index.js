// Example usage here, uncomment the following in actual usage
// import api from './helper';

export const fetchDogs = () => {
  return fetch('https://5dd272c56625890014a6da46.mockapi.io/dogs')
    .then(response => response.json())
    .then(responseJson => {
      return responseJson;
    })
    .catch(error => {
      console.error(error);
    });
};
