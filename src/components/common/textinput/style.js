import {StyleSheet} from 'react-native';
import * as Colors from 'themes/colors';
import {normalize} from 'utils/size';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    borderWidth: normalize(1),
    borderColor: Colors.border,
    borderRadius: normalize(5),
    padding: normalize(5),
  },
  title: {
    fontFamily: 'ProximaNovaT-Thin',
    marginBottom: normalize(5),
  },
  rightIconContainer: {
    width: normalize(24),
    height: normalize(24),
    marginLeft: normalize(6),
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightTextContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightText: {
    color: Colors.text,
  },
  leftIconContainer: {
    width: normalize(24),
    height: normalize(24),
    marginRight: normalize(6),
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: normalize(20),
    height: normalize(20),
    resizeMode: 'contain',
  },
  textInputContainer: {
    flex: 1,
  },
  textInput: {
    fontFamily: 'ProximaNova-Bold',
    fontSize: normalize(16),
    color: Colors.text,
    height: normalize(24),
    padding: 0,
  },
  error: {
    paddingVertical: normalize(5),
    paddingBottom: normalize(10),
  },
});
