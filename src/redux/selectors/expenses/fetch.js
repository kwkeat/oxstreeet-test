export const NAME = 'EXPENSES';

export const getExpensesList = store => store[NAME].fetch.data;
