import React from 'react';
import {Text as RNText, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import * as Colors from 'themes/colors';
import {normalize} from 'utils/size';

const Text = props => {
  const {
    style,
    children,
    xsmall,
    small,
    medium,
    fs18,
    large,
    xlarge,
    bold,
    thin,
    primary,
    secondary,
    gray,
    white,
    error,
    boulder,
    elevated,
    centered,
    flex,
    underline,
    ...otherProps
  } = props;
  return (
    <RNText
      allowFontScaling={false}
      style={[
        styles.text,
        xsmall && {fontSize: normalize(10)},
        small && {fontSize: normalize(12)},
        medium && {fontSize: normalize(16)},
        fs18 && {fontSize: normalize(18)},
        large && {fontSize: normalize(20)},
        xlarge && {fontSize: normalize(26)},
        primary && {color: Colors.primary},
        white && {color: Colors.white},
        error && {color: Colors.textError},
        boulder && {color: Colors.textBoulder},
        secondary && {color: Colors.secondary},
        bold && {fontFamily: 'ProximaNova-Bold'},
        thin && {fontFamily: 'ProximaNovaT-Thin'},
        elevated && styles.shadow,
        centered && {textAlign: 'center'},
        flex && {flex: 1},
        underline && {textDecorationLine: 'underline'},
        style,
      ]}
      {...otherProps}>
      {children}
    </RNText>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: normalize(14),
    color: Colors.textPrimary,
    fontFamily: 'ProximaNova-Regular',
  },
  shadow: {
    textShadowColor: '#000',
    textShadowOffset: {
      width: 0,
      height: 1,
    },
    textShadowRadius: 4,
  },
});

Text.propTypes = {
  children: PropTypes.any,
  style: PropTypes.any,
  xsmall: PropTypes.bool,
  small: PropTypes.bool,
  medium: PropTypes.bool,
  fs18: PropTypes.bool,
  large: PropTypes.bool,
  xlarge: PropTypes.bool,
  bold: PropTypes.bool,
  thin: PropTypes.bool,
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  gray: PropTypes.bool,
  white: PropTypes.bool,
  error: PropTypes.bool,
  boulder: PropTypes.bool,
  elevated: PropTypes.bool,
  centered: PropTypes.bool,
  flex: PropTypes.bool,
  underline: PropTypes.bool,
};

Text.defaultProps = {
  children: null,
  style: null,
  xsmall: false,
  small: false,
  medium: false,
  fs18: false,
  large: false,
  xlarge: false,
  bold: false,
  thin: false,
  primary: false,
  secondary: false,
  gray: false,
  white: false,
  error: false,
  boulder: false,
  elevated: false,
  centered: false,
  flex: false,
  underline: false,
};

export default Text;
