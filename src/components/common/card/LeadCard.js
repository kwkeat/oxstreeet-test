import React, {Component} from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import Card from 'common/card';
import Text from 'common/Text';
import {normalize} from 'utils/size';
import * as Colors from 'themes/colors';

class LeadCard extends Component {
  render() {
    const {navigation, leadDetails} = this.props;
    return (
      <Card style={styles.container} roundedCorner={10}>
        <View style={styles.line} />
        <View>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('LeadDetails', {leadDetails});
            }}>
            <View style={styles.nameWrapper}>
              <Icon name="mars" size={normalize(20)} />
              <Text style={styles.name} medium>
                {leadDetails && leadDetails.name}
              </Text>
            </View>
            <Text medium thin style={styles.text}>
              {leadDetails && `+6${leadDetails.phoneNumber}`}
            </Text>
            <View style={styles.ellipsisWrapper}>
              <Text medium thin style={styles.text}>
                {leadDetails && `${leadDetails.age} yrs old`}
              </Text>
              <Icon
                name="ellipsis-h"
                color={Colors.ellipsis}
                size={normalize(20)}
              />
            </View>
            <View style={{flexDirection: 'row', marginTop: normalize(10)}}>
              {leadDetails &&
                leadDetails.tags &&
                leadDetails.tags.map(tag => (
                  <Text
                    style={[
                      styles.tag,
                      {marginHorizontal: 0, marginRight: normalize(3)},
                    ]}
                    white>
                    {tag}
                  </Text>
                ))}
            </View>
          </TouchableOpacity>
        </View>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginVertical: normalize(10),
    padding: normalize(15),
  },
  line: {
    borderWidth: normalize(3),
    borderColor: Colors.statusPass,
    width: '60%',
    top: 0,
    left: 0,
    position: 'absolute',
  },
  text: {
    paddingVertical: normalize(2),
  },
  nameWrapper: {
    flexDirection: 'row',
    paddingBottom: normalize(10),
  },
  ellipsisWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  name: {
    paddingHorizontal: normalize(10),
  },
  description: {
    paddingTop: normalize(10),
  },
  tag: {
    backgroundColor: Colors.primary,
    padding: normalize(5),
    paddingHorizontal: normalize(10),
    marginHorizontal: normalize(3),
    borderRadius: normalize(14),
    overflow: 'hidden',
  },
});

LeadCard.propTypes = {
  navigation: PropTypes.object.isRequired,
  leadDetails: PropTypes.object.isRequired,
};

export default LeadCard;
