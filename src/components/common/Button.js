import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, TouchableOpacity} from 'react-native';
import * as Colors from 'themes/colors';
import Text from 'common/Text';
import {normalize} from 'utils/size';

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: normalize(10),
    paddingVertical: normalize(15),
    paddingHorizontal: normalize(16),
    marginVertical: normalize(5),
    minWidth: normalize(120),
    minHeight: normalize(14),
  },
  // eslint-disable-next-line react-native/no-unused-styles
  primary: {
    backgroundColor: Colors.primary,
  },
  // eslint-disable-next-line react-native/no-unused-styles
  secondary: {
    backgroundColor: Colors.secondary,
  },
  label: {
    color: Colors.white,
  },
});

const Button = props => {
  const {style, label, labelColor, type, onPress, isLoading} = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.button, styles[type], style]}>
      <Text style={[styles.label, {color: labelColor}]}>{label}</Text>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  onPress: PropTypes.func.isRequired,
  style: PropTypes.any,
  label: PropTypes.string.isRequired,
  labelColor: PropTypes.string,
  type: PropTypes.string,
  isLoading: PropTypes.bool,
};

Button.defaultProps = {
  style: null,
  type: 'primary',
  labelColor: Colors.white,
  isLoading: false,
};

export default Button;
