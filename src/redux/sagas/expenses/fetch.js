import {takeLatest, all, fork, put, call} from 'redux-saga/effects';
import * as api from 'api';
import Actions from 'actions';
import firestore from '@react-native-firebase/firestore';

function* fetchExpenses() {
  try {
    const ref = firestore().collection('expenses');
    const responseTransaction = yield call(api.fetchTransactions);
    console.log('response');
    console.log(responseTransaction);

    const response = yield ref.get();
    const data = response.docs.map(function(value) {
      return value._data;
    });
    if (data) {
      yield put(Actions.fetchExpensesSuccess(data));
    }
  } catch (error) {
    yield put(Actions.fetchExpensesFail(error));
  }
}

function* watchFetchExpenses() {
  yield takeLatest(Actions.FETCH_EXPENSES, fetchExpenses);
}

export default function* fetch() {
  yield all([fork(watchFetchExpenses)]);
}
