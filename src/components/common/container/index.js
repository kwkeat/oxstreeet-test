import React from 'react';
import {StyleSheet, View, ScrollView} from 'react-native';
import PropTypes from 'prop-types';
import * as Colors from 'themes/colors';
import StatusBar from 'common/nav/StatusBar';

const Container = ({containerStyle, children, scrollable}) => {
  const Component = scrollable ? ScrollView : View;
  return (
    <View style={styles.container}>
      <StatusBar />
      <Component
        contentContainerStyle={styles.scrollable}
        style={[styles.container, containerStyle]}>
        {children}
      </Component>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  scrollable: {
    flexGrow: 1,
  },
});

Container.propTypes = {
  containerStyle: PropTypes.any,
  children: PropTypes.node.isRequired,
  scrollable: PropTypes.bool,
};

Container.defaultProps = {
  containerStyle: {},
  scrollable: false,
};

export default Container;
