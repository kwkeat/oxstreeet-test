export const NAME = 'DOGS';

export const FETCH_DOGS = `${NAME}/FETCH_DOGS`;
export const FETCH_DOGS_SUCCESS = `${NAME}/FETCH_DOGS_SUCCESS`;
export const FETCH_DOGS_FAIL = `${NAME}/FETCH_DOGS_FAIL`;

export const fetchDogs = () => ({
  type: FETCH_DOGS,
});

export const fetchDogsSuccess = data => ({
  type: FETCH_DOGS_SUCCESS,
  data,
});

export const fetchDogsFail = error => ({
  type: FETCH_DOGS_FAIL,
  error,
});
