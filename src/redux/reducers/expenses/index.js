import {combineReducers} from 'redux';
import fetch from './fetch';
import create from './create';

export default combineReducers({
  fetch,
  create,
});
