import React, {Component} from 'react';
import {View, Image, StyleSheet} from 'react-native';
import NavBar from 'common/NavBar';
import Text from 'common/Text';
import * as Colors from 'themes/colors';
import {normalize} from 'utils/size';

class DogDetails extends Component {
  render() {
    const {navigation} = this.props;
    const dogDetails = navigation.getParam('dogDetails', {});
    return (
      <View style={styles.container}>
        <NavBar onLeftIconPressed={() => navigation.goBack()} />
        <View style={styles.contentContainer}>
          <Text style={styles.title}>{dogDetails.name}</Text>
          <Image source={{uri: dogDetails.image}} style={styles.image} />
          <Text style={styles.description}>{dogDetails.description}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: normalize(20),
    fontWeight: 'bold',
  },
  image: {
    width: normalize(250),
    height: normalize(240),
    alignSelf: 'center',
  },
  description: {
    fontSize: normalize(14),
    padding: normalize(15),
  },
});

export default DogDetails;
