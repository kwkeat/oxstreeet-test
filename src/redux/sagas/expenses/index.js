import {all, fork} from 'redux-saga/effects';
import fetch from './fetch';
import create from './create';

export default function* auth() {
  yield all([fork(fetch), fork(create)]);
}
