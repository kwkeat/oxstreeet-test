import React, {Component} from 'react';
import {connect} from 'react-redux';
import Actions from 'actions';
import Selectors from 'selectors';
import {
  View,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import Text from 'common/Text';
import * as Colors from 'themes/colors';
import {normalize} from 'utils/size';

class DogsList extends Component {
  componentDidMount() {
    const {fetchDogs} = this.props;
    fetchDogs();
  }

  renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() =>
          this.props.navigation.navigate('DogDetails', {
            dogDetails: item,
          })
        }>
        <Text style={styles.title}>{item.name}</Text>
        <Image source={{uri: item.image}} style={styles.image} />
        <Text numberOfLines={2} style={styles.description}>
          {item.description}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    const {dogsList} = this.props;

    console.log(dogsList);

    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={Colors.oxstreet} barStyle="light-content" />
        <Text style={styles.header} large bold>
          Welcome to Ox Street
        </Text>
        <Text>Doggie List</Text>
        <View style={styles.flatList}>
          <FlatList
            data={dogsList}
            renderItem={this.renderItem}
            keyExtractor={item => item.id}
            numColumns={2}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.lightGray,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    width: '100%',
    color: Colors.white,
    backgroundColor: Colors.oxstreet,
    justifyContent: 'center',
    textAlign: 'center',
    paddingVertical: normalize(20),
  },
  flatList: {
    flex: 1,
    marginBottom: 10,
    paddingHorizontal: normalize(10),
  },
  itemContainer: {
    width: '50%',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: Colors.gray,
  },
  title: {
    fontSize: normalize(18),
    fontWeight: 'bold',
    textAlign: 'right',
    marginRight: normalize(5),
  },
  description: {
    padding: normalize(15),
  },
  image: {
    width: normalize(170),
    height: normalize(160),
    alignSelf: 'center',
  },
});

const mapStateToProps = store => ({
  dogsList: Selectors.getDogsList(store),
});

const mapDispatchToProps = {
  fetchDogs: Actions.fetchDogs,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DogsList);
