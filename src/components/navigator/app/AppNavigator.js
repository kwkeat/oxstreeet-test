import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import Dogs from 'navigator/dogs';

const routeConfiguration = {
  Dogs: {screen: Dogs},
};

const AppNavigator = createSwitchNavigator(routeConfiguration);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
