import React, {useState, useEffect} from 'react';
import AppNavigator from 'navigator/app';

function App() {
  return <AppNavigator />;
}

export default App;
