export const NAME = 'EXPENSES';

export const FETCH_EXPENSES = `${NAME}/FETCH_EXPENSES`;
export const FETCH_EXPENSES_SUCCESS = `${NAME}/FETCH_EXPENSES_SUCCESS`;
export const FETCH_EXPENSES_FAIL = `${NAME}/FETCH_EXPENSES_FAIL`;

export const fetchExpenses = () => ({
  type: FETCH_EXPENSES,
});

export const fetchExpensesSuccess = data => ({
  type: FETCH_EXPENSES_SUCCESS,
  data,
});

export const fetchExpensesFail = error => ({
  type: FETCH_EXPENSES_FAIL,
  error,
});
