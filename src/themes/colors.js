export const primary = '#4F8CF0';
export const secondary = '#1E90FF';
export const white = 'white';
export const transparent = 'transparent';
export const ellipsis = '#3291E3';
export const purple = '#A84DE2';
export const green = '#1AD855';
export const yellow = '#FADA5E';
export const red = '#ED4337';
export const oxstreet = '#768067';

export const gray = '#D4D4D6';
export const lightGray = '#D4D4D630';
export const text = '#707070';
export const background = '#4F8CF0';

export const actionButton = '#3DA2D9';

// Lead Card
export const statusPass = '#32DDC8';
export const statusIdle = '#A2C7F8';
export const statusReject = '#F49B97';

// TextInput
export const border = '#D7D7D7';

// Contact
export const chateauGreen = '#4ABF58';
export const mojo = '#CD4A35';
export const dodgerBlue = '#1683F4';

// Text
export const textPrimary = '#333333';
export const textBoulder = '#747474';
export const textSilver = '#BFBFBF';
export const textError = '#FF0000';

// Settings
export const sail = '#A2C7F8';
