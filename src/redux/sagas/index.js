import {all, fork} from 'redux-saga/effects';
import auth from './auth';
import expenses from './expenses';
import dogs from './dogs';
import common from './common';

export default function* root() {
  yield all([fork(auth), fork(expenses), fork(common), fork(dogs)]);
}
