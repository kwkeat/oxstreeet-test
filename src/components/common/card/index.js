import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet} from 'react-native';
import ElevatedView from 'react-native-elevated-view';
import * as Colors from 'themes/colors';
import {normalize} from 'utils/size';

class Card extends Component {
  render() {
    const {
      style,
      elevation,
      roundedCorner,
      cardRef,
      children,
      ...otherProps
    } = this.props;
    return (
      <ElevatedView
        elevation={elevation}
        style={[styles.elevatedStyle, style]}
        ref={cardRef}
        {...otherProps}>
        {children}
      </ElevatedView>
    );
  }
}

const styles = StyleSheet.create({
  elevatedStyle: {
    width: '100%',
    height: null,
    // borderRadius: normalize(10),
    backgroundColor: Colors.white,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.gray,
  },
});

Card.propTypes = {
  style: PropTypes.any,
  elevation: PropTypes.number,
  roundedCorner: PropTypes.number,
  children: PropTypes.node.isRequired,
  cardRef: PropTypes.any,
};

Card.defaultProps = {
  style: styles.container,
  elevation: 5,
  roundedCorner: null,
  cardRef: null,
};

export default Card;
