import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Platform,
  Dimensions,
  Image,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as Colors from 'themes/colors';
import {normalize} from 'utils/size';
import Text from 'common/Text';

const {height: SCREEN_HEIGHT} = Dimensions.get('window');

const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
const HEADER_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 105 : 81) : 60;
const NAV_BAR_HEIGHT = HEADER_HEIGHT - STATUS_BAR_HEIGHT;

const styles = StyleSheet.create({
  container: {
    height: HEADER_HEIGHT,
    position: 'relative',
  },
  statusBar: {
    height: STATUS_BAR_HEIGHT,
    backgroundColor: Colors.oxstreet,
  },
  navBar: {
    height: NAV_BAR_HEIGHT,
    backgroundColor: Colors.oxstreet,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: STATUS_BAR_HEIGHT,
    left: 0,
    right: 0,
    shadowRadius: 1,
    shadowOpacity: 0.7,
    shadowOffset: {width: 0, height: 4},
    shadowColor: 'rgba(191,191,191,0.4)',
    elevation: 4,
    borderColor: 'transparent',
    borderWidth: normalize(1),
  },
  elementRight: {
    position: 'absolute',
    right: 20,
    top: 5,
    bottom: 15,
    minWidth: 50,
    alignItems: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingBottom: normalize(5),
  },
  secondaryRightIcon: {
    paddingRight: normalize(20),
  },
  icon: {
    resizeMode: 'contain',
    width: normalize(30),
    height: normalize(30),
  },
  iconLeft: {
    position: 'absolute',
    left: 0,
    top: 5,
    bottom: 0,
    minWidth: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconRight: {
    resizeMode: 'contain',
    width: normalize(30),
    height: normalize(30),
  },
  border: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.gray,
  },
});

const images = {
  backArrow: require('icons/back-arrow.png'),
  filter: require('icons/leads/filter.png'),
};

class NavBar extends Component {
  renderLeftElement() {
    const {onLeftIconPressed, leftIconName, leftIconColor} = this.props;

    if (onLeftIconPressed) {
      return (
        <TouchableOpacity style={styles.iconLeft} onPress={onLeftIconPressed}>
          {/* <Icon
            name={leftIconName}
            size={normalize(16)}
            color={leftIconColor}
          /> */}
          <Image source={images.backArrow} style={styles.icon} />
        </TouchableOpacity>
      );
    }

    return false;
  }

  renderRightElement() {
    const {
      onRightElementPressed,
      rightElementColor,
      rightIconName,
      rightTextName,
      rightElement,
      rightElementStyle,
      secondaryRightIconName,
      onSecondaryRightElementPressed,
    } = this.props;

    return (
      <View style={[styles.elementRight, rightElementStyle]}>
        <TouchableOpacity
          style={styles.secondaryRightIcon}
          onPress={onSecondaryRightElementPressed}>
          <Icon
            name={secondaryRightIconName}
            size={25}
            color={rightElementColor}
          />
        </TouchableOpacity>
        {onRightElementPressed && (
          <TouchableOpacity onPress={onRightElementPressed}>
            <Image source={images.filter} style={styles.iconRight} />
          </TouchableOpacity>
        )}
        {rightTextName && <Text>{rightTextName}</Text>}
        {rightElement !== null && rightElement}
      </View>
    );
  }

  render() {
    const {
      title,
      titleStyle,
      navBarStyle,
      statusBarStyle,
      isLeftIconShow,
      showBorder,
      containerStyle,
    } = this.props;

    return (
      <View style={[styles.container, containerStyle]}>
        <View style={[styles.statusBar, statusBarStyle]} />
        <View style={[styles.navBar, navBarStyle, showBorder && styles.border]}>
          {isLeftIconShow && this.renderLeftElement()}
          <Text medium bold white style={titleStyle}>
            {title}
          </Text>
          {this.renderRightElement()}
        </View>
      </View>
    );
  }
}

NavBar.propTypes = {
  title: PropTypes.string,
  titleStyle: PropTypes.any,
  leftIconName: PropTypes.string,
  rightIconName: PropTypes.string,
  secondaryRightIconName: PropTypes.string,
  rightTextName: PropTypes.string,
  leftIconColor: PropTypes.string,
  rightElementColor: PropTypes.string,
  onLeftIconPressed: PropTypes.func,
  onRightElementPressed: PropTypes.func,
  onSecondaryRightElementPressed: PropTypes.func,
  statusBarStyle: PropTypes.any,
  navBarStyle: PropTypes.any,
  rightElement: PropTypes.any,
  rightElementStyle: PropTypes.any,
  isLeftIconShow: PropTypes.bool,
  showBorder: PropTypes.bool,
  containerStyle: PropTypes.any,
};

NavBar.defaultProps = {
  title: '',
  titleStyle: null,
  leftIconName: 'arrow-left',
  isLeftIconShow: true,
  rightIconName: null,
  secondaryRightIconName: null,
  rightTextName: null,
  leftIconColor: Colors.white,
  rightElementColor: Colors.white,
  onLeftIconPressed: null,
  onRightElementPressed: null,
  onSecondaryRightElementPressed: null,
  statusBarStyle: {},
  navBarStyle: {},
  rightElement: null,
  rightElementStyle: null,
  showBorder: false,
  containerStyle: {},
};

export default NavBar;
