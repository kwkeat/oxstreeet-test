import Actions from 'actions';

const getDefaultState = () => ({isLoading: false, error: null});

function signUp(state, action) {
  if (typeof state === 'undefined') {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.SIGN_UP:
      return {
        isLoading: true,
        error: null,
      };
    case Actions.SIGN_UP_SUCCESS:
      return {
        isLoading: false,
        error: null,
      };
    case Actions.SIGN_UP_FAIL:
      return {
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export default signUp;
