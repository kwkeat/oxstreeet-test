import * as fetch from './fetch';
import * as create from './create';

export default {
  ...fetch,
  ...create,
};
