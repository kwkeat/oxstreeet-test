import React, {Component} from 'react';
import {View, TextInput, Image, TouchableOpacity} from 'react-native';
import Text from 'common/Text';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import {normalize} from 'utils/size';
import styles from './style';

class SocarTextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focus: false,
      passwordVisible: false,
    };
  }

  _toggleFocus = () => {
    this.setState({focus: !this.state.focus});
  };

  _togglePasswordVisible = () => {
    this.setState({passwordVisible: !this.state.passwordVisible});
  };

  render() {
    const {
      password: isPassword,
      hasError,
      title,
      rightText,
      leftIcon,
      rightIcon,
      password,
      placeholder,
      maxLength,
      error,
      editable,
      onRightElementPress,
      ...otherProps
    } = this.props;
    return (
      <View>
        <Text medium style={styles.title}>{`${title}:`}</Text>
        <View style={styles.container}>
          {leftIcon ? (
            <View style={styles.leftIconContainer}>
              <Text>Left</Text>
              {/* <Image style={styles.icon} source={icons[leftIcon]} /> */}
            </View>
          ) : (
            <View />
          )}
          <View style={styles.textInputContainer}>
            <TextInput
              style={styles.textInput}
              onFocus={this._toggleFocus}
              onBlur={this._toggleFocus}
              autoCorrect={false}
              autoCapitalize="none"
              editable={editable}
              placeholder={placeholder}
              maxLength={maxLength}
              {...(isPassword
                ? {
                    autoCompleteType: 'password',
                    secureTextEntry: !this.state.passwordVisible,
                  }
                : {})}
              {...otherProps}
            />
          </View>
          {rightIcon ? <View style={styles.rightIconContainer} /> : <View />}
          {rightIcon && (
            <TouchableOpacity
              onPress={onRightElementPress}
              style={styles.rightIconContainer}>
              <Icon name={rightIcon} size={normalize(20)} />
            </TouchableOpacity>
          )}
          {rightText !== '' && (
            <TouchableOpacity
              onPress={onRightElementPress}
              style={styles.rightTextContainer}>
              <Text semiBold medium style={styles.rightText}>
                {rightText}
              </Text>
            </TouchableOpacity>
          )}
        </View>
        {hasError && (
          <Text error style={styles.error}>
            {error}
          </Text>
        )}
      </View>
    );
  }
}

SocarTextInput.propTypes = {
  rightIcon: PropTypes.string,
  leftIcon: PropTypes.string,
  password: PropTypes.bool,
  date: PropTypes.bool,
  card: PropTypes.bool,
  maxLength: PropTypes.number,
  rightText: PropTypes.string,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  editable: PropTypes.bool,
  hasError: PropTypes.bool,
  onRightElementPress: PropTypes.func,
  title: PropTypes.string,
};

SocarTextInput.defaultProps = {
  leftIcon: null,
  rightIcon: null,
  rightText: '',
  title: 'Title',
  password: false,
  date: false,
  card: false,
  maxLength: null,
  placeholder: 'Placeholder',
  error: '',
  editable: true,
  onRightElementPress: null,
  hasError: true,
};

export default SocarTextInput;
