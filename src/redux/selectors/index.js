import auth from './auth';
import common from './common';
import dogs from './dogs';
import expenses from './expenses';
import persist from './persist';

export default {
  ...auth,
  ...common,
  ...expenses,
  ...dogs,
  ...persist,
};
