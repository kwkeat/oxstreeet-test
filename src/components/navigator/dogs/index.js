import {createStackNavigator} from 'react-navigation';
import DogsList from 'containers/transaction/DogsList';
import DogDetails from 'containers/transaction/DogDetails';

const routeConfiguration = {
  DogsList: {
    screen: DogsList,
  },
  DogDetails: {
    screen: DogDetails,
  },
};

const stackNavigatorConfiguration = {
  headerMode: 'none',
};

export default createStackNavigator(
  routeConfiguration,
  stackNavigatorConfiguration,
);
