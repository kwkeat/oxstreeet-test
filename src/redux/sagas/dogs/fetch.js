import {takeLatest, all, fork, put, call} from 'redux-saga/effects';
import * as api from 'api';
import Actions from 'actions';

function* fetchDogs() {
  try {
    const response = yield call(api.fetchDogs);
    const data = response.data;
    if (response && data) {
      yield put(Actions.fetchDogsSuccess(data));
    }
  } catch (error) {
    yield put(Actions.fetchDogsFail(error));
  }
}

function* watchFetchDogs() {
  yield takeLatest(Actions.FETCH_DOGS, fetchDogs);
}

export default function* fetch() {
  yield all([fork(watchFetchDogs)]);
}
