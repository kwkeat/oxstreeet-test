import React, {Component} from 'react';
import {StyleSheet, View, Dimensions, Image, Platform} from 'react-native';
import PropTypes from 'prop-types';
import {normalize} from 'utils/size';
import * as Colors from 'themes/colors';
import ElevatedView from 'react-native-elevated-view';

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');
const DEFAULT_SIZE = normalize(80);
const DEFAULT_RADIUS = DEFAULT_SIZE / 2;
const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
const HEADER_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 45;

const styles = StyleSheet.create({
  container: {
    height: DEFAULT_SIZE,
    width: DEFAULT_SIZE,
    borderRadius: DEFAULT_RADIUS,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    backgroundColor: Colors.white,
  },
  avatarStyle: {
    width: DEFAULT_SIZE,
    height: DEFAULT_SIZE,
    resizeMode: 'cover',
    overflow: 'hidden',
  },
});

class Avatar extends Component {
  render() {
    const {source, size, style} = this.props;
    return (
      <ElevatedView
        elevation={5}
        style={{
          backgroundColor: Colors.white,
          width: size,
          height: size,
          borderRadius: size / 2,
          ...style,
        }}>
        <View
          style={[
            styles.container,
            {width: size, height: size, borderRadius: size / 2},
          ]}>
          <Image
            source={source}
            style={[styles.avatarStyle, {width: size, height: size}]}
          />
        </View>
      </ElevatedView>
    );
  }
}

Avatar.propTypes = {
  source: PropTypes.any,
  size: PropTypes.number,
  style: PropTypes.any,
};

Avatar.defaultProps = {
  source: null,
  size: DEFAULT_SIZE,
  style: {},
};

export default Avatar;
