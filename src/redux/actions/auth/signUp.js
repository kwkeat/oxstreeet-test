export const NAME = 'AUTH';

export const SIGN_UP = `${NAME}/SIGN_UP`;
export const SIGN_UP_SUCCESS = `${NAME}/SIGN_UP_SUCCESS`;
export const SIGN_UP_FAIL = `${NAME}/SIGN_UP_FAIL`;

export const signUp = credentials => ({
  type: SIGN_UP,
  credentials,
});

export const signUpSuccess = token => ({
  type: SIGN_UP_SUCCESS,
  token,
});

export const signUpFail = error => ({
  type: SIGN_UP_FAIL,
  error,
});
